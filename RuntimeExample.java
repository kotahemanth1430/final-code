import java.io.IOException;
import java.lang.Runtime;

public class RuntimeExample {
    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();

        long totalMemory = runtime.totalMemory();
        System.out.println("Total Memory: " + totalMemory + " bytes");

        
        try {
            Process process = runtime.exec("ls -l");

            java.io.InputStream inputStream = process.getInputStream();
            java.util.Scanner scanner = new java.util.Scanner(inputStream).useDelimiter("\\A");
            String commandOutput = scanner.hasNext() ? scanner.next() : "";
            System.out.println("Command Output:");
            System.out.println(commandOutput);

            int exitCode = process.waitFor();
            System.out.println("Exit Code: " + exitCode);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        Thread shutdownHook = new Thread(() -> {
            System.out.println("Shutdown hook executed");
            
        });
        runtime.addShutdownHook(shutdownHook);

        // Run garbage collection
        runtime.gc();
    }
}
